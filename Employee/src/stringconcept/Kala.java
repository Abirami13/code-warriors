package stringconcept;

public class Kala {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s1="john";
		String s2="John";
		String s3=new String("john");
		String s4=new String("hello");
		String s5=new String("hello");
		System.out.println(s1==s2);
		System.out.println(s1==s3);
		System.out.println(s1.equals(s3));
		System.out.println(s1.equals(s2));
		System.out.println(s1.equalsIgnoreCase(s2));
		System.out.println(s4==s5);
		System.out.println(s4.equals(s5));
		s1.concat("rock");
		System.out.println(s1);
		s1=s1.concat("rock");
		char b=s1.charAt(2);
		System.out.println(b);
		StringBuffer sb=new StringBuffer("abc");
		System.out.println(sb.length());
        System.out.println(sb.capacity());
        StringBuilder builder=new StringBuilder("hello");  
        builder.append("java");  
        System.out.println(builder);
	}
}
