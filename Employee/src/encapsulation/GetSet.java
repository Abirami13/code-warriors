package encapsulation;

public class GetSet {

String name;
int id;
int age;

public int getAge() {
   return age;
}

public String getName() {
   return name;
}

public int getid() {
   return id;
}

public void setAge( int age) {
   this.age =age;
}

public void setName(String name) {
   this.name = name;
}

public void setid( int id) {
   this.id = id;
}
}